package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"sort"
        "io/ioutil"
        "log"
	"github.com/go-martini/martini"
	"github.com/layeh/gumble/gumble"
	"github.com/layeh/gumble/gumble_ffmpeg"
	"github.com/layeh/gumble/gumbleutil"
)

func update(dirname string, files map[string]string){
    for k := range files {
        delete(files, k)
    }
    dirfiles, err := ioutil.ReadDir(dirname)
    if err != nil {
        log.Fatal(err)
    }
    for _, f := range dirfiles {
        filename := f.Name()
	key := filepath.Base(filename)
	files[key] = dirname + filename
    }
}

func main() {
	var directory string
	files := make(map[string]string)
	var stream *gumble_ffmpeg.Stream

	gumbleutil.Main(func(_ *gumble.Config, client *gumble.Client) {
		var err error
		stream, err = gumble_ffmpeg.New(client)
		if err != nil {
			fmt.Printf("%s\n", err)
			os.Exit(1)
		}
	        directory = flag.Args()[0]
	        update(directory, files)

	}, gumbleutil.Listener{
		Connect: func(e *gumble.ConnectEvent) {
			fmt.Printf("GoMumbleSoundboard loaded (%d files)\n", len(files))
			fmt.Printf("Connected to %s\n", e.Client.Conn().RemoteAddr())
			if e.WelcomeMessage != "" {
				fmt.Printf("Welcome message: %s\n", e.WelcomeMessage)
			}
			fmt.Printf("Current Channel: %s\n", e.Client.Self().Channel().Name())

			m := martini.Classic()
			m.Get("/files.json", func() string {
				keys := make([]string, 0, len(files))
				for k := range files {
					keys = append(keys, k)
				}
				ss := sort.StringSlice(keys)
				ss.Sort()

				js, _ := json.Marshal(ss)
				return string(js)
			})
			m.Get("/play/:file", func(params martini.Params) (int, string) {
				file, ok := files[params["file"]]
				if !ok {
					return 404, "not found"
				}
				stream.Stop()
				if err := stream.Play(file); err != nil {
					return 400, fmt.Sprintf("%s\n", err)
				} else {
					return 200, fmt.Sprintf("Playing %s\n", file)
				}
			})
			m.Get("/stop", func() string {
				stream.Stop()
				return "ok"
			})
			m.Get("/update", func() string {
				update(directory, files)
			        return "ok"
			})
			m.Run()
		},
	})
}
